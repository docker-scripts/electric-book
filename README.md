# Electric Book container

https://electricbookworks.github.io/electric-book/

## Installation

  - First install `ds` and `revproxy`:
    + https://gitlab.com/docker-scripts/ds#installation
    + https://gitlab.com/docker-scripts/revproxy#installation

  - Then get the scripts: `ds pull electric-book`

  - Create a directory for the container: `ds init electric-book @ebook`

  - Fix the settings: `cd /var/ds/ebook/ ; vim settings.sh`

  - Make the container: `ds make`

## Try the sample book

To look at the sample book, add `127.0.0.1 book1.example.org` to
`/etc/hosts` then open in browser: https://book1.example.org/

Alternatively, you can run `ds ebook serve book1.example.org`,
then open http://127.0.0.1:4000 in browser.

The directory of this book is: `books/book1.example.org`.  If
you make some changes to it, you can rebuild the site with:
`ds ebook build books/book1.example.org`, or with:
`ds ebook build books/book1.example.org --incremental`

To update the gems and npm packages of this book, run:
`ds ebook update books/book1.example.org`

## Add another book

We can create another book like this:
```
ds ebook init books/book2
ds ebook install books/book2
ds ebook render books/book2
ds site add book2.example.org books/book2
```

If the domain is not a real one, add this line in `/etc/hosts`:
`127.0.0.1 book2.example.org`

To try it locally, run: `ds ebook serve books/book2` and then open in
browser http://127.0.0.1:4000

**Note:** If you have many books, it might be better to name the
book the same as the domain, in order to avoid any confusion when
managing them. For example: `ds ebook init books/book2.example.org`

## Remove a book

We can remove a book like this:
- First delete the apache2 site: `ds site del book1.example.org`
- Then remove the book: `rm -rf books/book1.example.org`

## Export to PDf etc

```
ds ebook export-print-pdf books/book2
ds ebook export-screen-pdf books/book2
ds ebook export-epub books/book2
ds ebook export-word books/book2
ds ebook scripts books/book2
```

## Serve multiple books from the same site/domain

If you want to serve multiple books from the same site/domain, like
https://books.example.org/book-1/, https://books.example.org/book-2/,
https://books.example.org/book-3/, etc, you can do it like this:
```
ds ebook init books/sample
ds ebook install books/sample
ds ebook render books/sample

cp -a books/sample books/book-1
cp -a books/sample books/book-2
cp -a books/sample books/book-3
rm -rf books/sample

mkdir books/books.example.org
cd books/books.example.org/
ln -s ../book-1/www book-1
ln -s ../book-2/www book-2
ln -s ../book-3/www book-3
cd -

ds site add books.example.org books/books.example.org --multibook
# don't forget the --multibook option at the end
```

You also need to edit the config file `books/book-1/_config.yml` and
set `baseurl: "/book1"`. Do the same for the other books, and then
render (generate) the html pages again:
```
ds ebook render books/book-1
ds ebook render books/book-2
ds ebook render books/book-3
```

## Other commands

```
ds stop
ds start
ds shell
ds help
```
