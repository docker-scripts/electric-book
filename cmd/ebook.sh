cmd_ebook_help() {
    cat <<_EOF
    ebook <command> <dir>
        Run an ebook command on the given book directory.
        The command can be one of:
            install/update, render/build, serve, scripts,
            export-print-pdf, export-screen-pdf, export-word, export-epub

_EOF
}

cmd_ebook() {
    local cmd=$1;  shift
    local dir=$1;  shift
    [[ -z $dir ]] && echo -e "Usage:\n$(cmd_ebook_help)\n" && exit 1

    case $cmd in
        init)
            set -x
            git clone https://github.com/electricbookworks/electric-book.git $dir
            rm -rf $dir/.git
            chmod +x $dir/run-linux.sh
            sed -i $dir/_config.yml \
                -e 's/^canonical-url:/#canonical-url:/' \
                -e 's/^baseurl:/#baseurl:/'
            set +x
            ;;
        install|update)
            ds exec bash -c "cd $dir; bundle update; bundle install; npm install"
            ;;
        render|build)
            ds exec bash -c \
               "cd $dir; \
                LC_ALL=en_US.UTF-8 bundle exec jekyll build --destination www $@"
            ;;
        serve)
            ds exec bash -c \
               "cd $dir;
                LC_ALL=en_US.UTF-8 bundle exec \
                jekyll serve --host 0.0.0.0 --incremental $@"
            ;;
        scripts)
            ds exec bash -c \
               "cd $dir ;
                xdg-open() { : ; } ;
                export -f xdg-open ;
                export LC_ALL=en_US.UTF-8 ;
                ./run-linux.sh"
            ;;
        export-print-pdf)   _export 1 ;;
        export-screen-pdf)  _export 2 ;;
        export-epub)        _export 4 ;;
        export-word)        _export 6 ;;

        *)
            echo -e "Usage:\n$(cmd_serve_help)\n"
            exit 1
            ;;
    esac
}

_export() {
    local type=$1
    ds exec bash -c \
       "cd $dir ;
        xdg-open() { : ; } ;
        export -f xdg-open ;
        export LC_ALL=en_US.UTF-8 ;
        echo -e '$type\n\n\n\n\nx\nx\n' | ./run-linux.sh"
}
